import { MovieInput } from "../input-types/MovieInput";
import { Document } from "dynamoose/dist/Document";

export interface IMovie extends Document {
    id: string
    name: string
    description: string
    director: string
    actor: string
    actress: string
}


export interface IData {
    getMovie(): Promise<IMovie[]>
    addMovie(movieInput: MovieInput): Promise<IMovie>
    getMovieById(movieId: String): Promise<IMovie>
    updateMovieById(updateMovieId: MovieInput): Promise<IMovie>
    deleteMovieById(deleteMovieId: String): Promise<String>
}
///*
export interface ICast {
    actor: string
    actress: string
}
//*/

export type Cast = {
    actor: string
    actress: string
}