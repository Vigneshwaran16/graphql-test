import {  IMovie } from "./data-access";
import { MovieModel } from '../db/MovieModel'
import { MovieInput } from "../input-types/MovieInput";
import { v4 } from "uuid";
//import {Movie} from '../types/Movie'

export class DAO {
    async getMovie(): Promise<any> {
        
        try{
            let result = await MovieModel.scan().exec()
            
            result = JSON.parse(JSON.stringify(result))
            console.log('Ggggettallll--------')
            console.log(result)
            return result
        } catch(err){
            console.log(err)
            return 
        }
    }

    async addMovie(movieInput: MovieInput): Promise<IMovie> {
        movieInput.id = v4()
        /*
        let movie: IMovie = {}
        movie = await MovieModel.create({...movieInput})

        return movie
        */
       const movie: IMovie = await MovieModel.create({ ...movieInput})
       //console.table(movie)
       return movie
    }
    
    async getMovieById(movieId: string): Promise<IMovie> {
        const movie: IMovie = await MovieModel.get({id: movieId})
        return movie
    }

    async updateMovie(updateMovieInput: MovieInput): Promise<IMovie> {
        const uuid = updateMovieInput.id
        let updateData: any = updateMovieInput
        delete updateData.id
        const updatedMovie = await MovieModel.update({id: uuid}, {...updateData}, { return: "document"})
        return updatedMovie
    }

    async deleteMovieById(movieId: string): Promise<String>{
        let response = ''
        try{
            await MovieModel.delete({id: movieId})
            response = 'Deleted movie successfully'
        }catch(err){
            response = err.message
        }

        return response
    }
}   