import 'reflect-metadata'
import  { ApolloServer } from 'apollo-server-express'
//import { buildSchemaSync } from 'type-graphql'
import { buildFederatedSchema } from './helper/buildFederatedSchema'
import express,{ Application } from 'express'
import { MovieResolver }  from './resolvers/MovieResolvers'
import { dynamodb } from './db/DbConfig'
import { TheaterResolver } from './resolvers/TheaterResolvers'
import { Theater } from './types/Theater'
import { Movie } from './types/Movie'
import { resolveMovieReference } from './resolvers/movie-reference'
/*
class App {
    private app: Application
    private schema
    private port
    private path 
    private apolloServer: ApolloServer
    constructor(){
        this.port = process.env.PORT || 4000
        this.path = '/graphql'
        this.app = express()
        this.schema = this.buildSchema()
        dynamodb()
        this.createServer()
        this.initMiddleware(this.app, this.path)
    }

    private buildSchema(){
        const schema = buildSchemaSync({
            resolvers: [MovieResolver]
        })
        return schema
    }

    private createServer(){
        this.apolloServer = new ApolloServer({
            schema: this.schema,
            playground: true
        })
    }

    private initMiddleware(app: Application, path: string){
        this.apolloServer.applyMiddleware({app, path})
    }

    public listen(){
        this.app.listen(this.port, () => {
            console.log(`Server listening on ${this.port}`)
        })
    }

}

const expressApp = new App()
expressApp.listen()
*/

///*
(async () =>{
    const path = '/graphql'
    const port = 4000
    const app: Application = express()

    const schema = await buildFederatedSchema(
        {
          resolvers: [MovieResolver, TheaterResolver],
          orphanedTypes: [Movie , Theater],
        },
        {
          Media: { __resolveReference: resolveMovieReference },
        },
    );

    dynamodb()
    const apolloServer = new ApolloServer({
        schema: schema,
        playground: true
    })

    //app.use(path)
    apolloServer.applyMiddleware({app, path})

    app.listen(port, ()  => {
        console.log(`Server listening on ${port}`)
    })
})()
//*/
