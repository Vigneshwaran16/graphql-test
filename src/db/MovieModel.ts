import { Schema, model } from 'dynamoose';

import { IMovie } from '../datasources/data-access'
export const schema = new Schema(
  {
    id: {
      type: String,
      hashKey: true,
    },
    name: String,
    description: String,
    director: String,
    actor: String,
    actress: String
  },
  {
    timestamps: true,
  }
);

export   const MovieModel = model<IMovie>('Movie', schema, {
   "create": true,
    throughput: {
        read: 5,
        write: 5,
    },
});