import * as dynamoose from 'dynamoose';

var AWS = require('aws-sdk');

dynamoose.aws.sdk.config.update({
    accessKeyId:"foo",
    secretAccessKey:"bar",
    region:"localhost",      
  })

AWS.config.update({endpoint: "http://localhost:8000"});

export const dynamodb = async() => {
    try {
         await dynamoose.aws.ddb
        .local
        ("http://localhost:8000")        
    
        console.log('Dynamodb connected.....')
    } catch (err) {
        console.error(err.message);
        process.exit(1)
    }
}

