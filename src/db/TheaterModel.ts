import { Schema, model } from 'dynamoose'

export const schema  = new Schema({
    id: String,
    screenName: String,
    startTime: String,
    endTime: String,
    movie: {
        type: Object,
        schema: {
            name: String
        }
    }
})

export const TheaterModel = model('Theater', schema)