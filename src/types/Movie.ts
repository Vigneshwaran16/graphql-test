//import { IsUUID } from 'class-validator'
//import { Document } from 'dynamoose/dist/Document';
import { Field, ObjectType }  from 'type-graphql'
//import {  Cast } from '../datasources/data-access'

@ObjectType()
export class Movie {

    @Field(() => String)
    //@IsUUID('4')
    id: string

    @Field({nullable: true})
    name: string

    @Field()
    description: string

    @Field()
    director: string

    @Field()
    actor: string

    @Field()
    actress: string

}

