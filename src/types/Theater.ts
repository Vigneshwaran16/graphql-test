import { Document } from "dynamoose/dist/Document";
import { Field, ObjectType } from "type-graphql";
import { Movie } from './Movie'

@ObjectType()
export class Theater extends Document{
    @Field()
    id: String

    @Field()
    screenName: String

    @Field()
    movie: Movie

    @Field()
    startTime: String

    @Field()
    endTime: String
}