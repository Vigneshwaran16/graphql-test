import { Field, InputType } from 'type-graphql';
import { Movie } from '../types/Movie'


@InputType({ description: 'New Movie'})
export class MovieInput implements Partial<Movie> {

    @Field(() => String)
    id: string

    @Field()
    name: string

    @Field()
    description: string

    @Field()
    director: string

    @Field()
    actor: string

    @Field()
    actress: string

}





export class UpdateMovie implements Partial<Movie> {


}