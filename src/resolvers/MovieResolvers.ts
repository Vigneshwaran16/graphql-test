import { IData, IMovie } from "../datasources/data-access";
import { Movie } from "../types/Movie";
import { Mutation, Query, Resolver, Arg } from "type-graphql";
import {DAO} from '../datasources/data-dao'
import { MovieInput } from "../input-types/MovieInput";

@Resolver(() => Movie)
export class MovieResolver implements IData {
    private dao

    constructor(){
        this.dao = new DAO()
    }

    @Query(() => Movie )
    async getMovie(): Promise<IMovie[]>{
        return await this.dao.getMovie()
    }  

    @Query( () => Movie )
    async getMovieById(@Arg('movieId') movieId: string): Promise<IMovie> {
        return this.dao.getMovieById(movieId)
    }
    
    @Mutation( () => Movie)
    async addMovie(@Arg('movieData') movieInput: MovieInput): Promise<IMovie>{
        return await this.dao.addMovie(movieInput)
    }
    @Mutation ( () => Movie)
    async  updateMovieById(@Arg('movieId') updateMovieInput: MovieInput): Promise<IMovie> {
        return await this.dao.updateMovie(updateMovieInput)
    }

    @Mutation( () => Movie)
    async deleteMovieById( @Arg('movieId') movieId: string): Promise<String> {
        return await this.dao.deleteMovieById(movieId)
    }
}